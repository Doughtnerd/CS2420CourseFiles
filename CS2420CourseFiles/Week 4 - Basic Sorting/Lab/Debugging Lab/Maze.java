package lab03;

import java.util.List;
import java.util.Properties;

import codemaze.Backpack;
import codemaze.Checkpoint;
import codemaze.Constants;
import codemaze.SpawnPoint;
import codemaze.binarysearch.Cauldron;
import codemaze.conditional.PuzzleWall;
import codemaze.conditional.PuzzleWall.Tumbler;
import codemaze.firsthurdle.Entrance;
import codemaze.sphinx.Sphinx;
import codemaze.spin.TeaCupCode;
import codemaze.spin.TeaCups;

public class Maze {
	Backpack backpack = new Backpack();

	public void enterMaze() {
		System.out.println(Entrance.getEntranceMessage());
		String uidPropertyFileName = "maze.properties";
		Properties uidPropertyfile = Entrance.readPropertyFile(uidPropertyFileName);
		String uid = uidPropertyfile.getProperty("uid");
		Entrance.declareUID(uid);
	}

	public void potionProblem() {
		System.out.println(Constants.Cauldron.GREETING);
		backpack.setBinarySearch(this::binarySearch);
		Cauldron.testBinarySearch(backpack);
	}
	
	public void aroundWeGo() {
		System.out.println(Constants.TeaCups.GREETING);
		TeaCups cups = new TeaCups(this::teacups);
		backpack.checkCode(cups.getCode());
	}
	
	public void sphnix() {
		System.out.println(Constants.Sphinx.GREETING);
		Sphinx cyborgSphinx = new Sphinx(this::checkPassword);
		backpack.debugSphinx(cyborgSphinx, "password");
	}
	
	public void puzzleWall() {
		System.out.println(Constants.PuzzleWall.GREETING);
		PuzzleWall puzzWall = new PuzzleWall();
		List<Tumbler> outerRing = puzzWall.generateTumblers(26);
		List<Tumbler> middleRing = puzzWall.generateTumblers(23);
		List<Tumbler> innerRing = puzzWall.generateTumblers(20);
		System.out.println(Constants.PuzzleWall.EXPLANATION);
		puzzWall.scrambleTumblers(outerRing, middleRing, innerRing);
		puzzWall.tryPassword(calculatePuzzlePassword(outerRing, middleRing, innerRing));
	}
	
	
	private TeaCupCode teacups() {
		int a = 1, b = 0, c = -1, d = 3, f = 5;

		//Loop #1
		while(a < 10) {
			a += d;
			d -= c;
			a++;
			try {
				Thread.sleep(100);
			} catch(InterruptedException e) { }
		}

		//Loop #2
		while(b < 10) {
			b *= a;
			a += c;
			d++;
			try {
				Thread.sleep(100);
			} catch(InterruptedException e) { }
		}

		//Loop #3
		while(a > c) {
			a += d;
			d--;
			b++;
			try {
				Thread.sleep(100);
			} catch(InterruptedException e) { }
		}

		//Loop #4
		while(f > 0) {
			f -= c;
			b += c;
			c *= -1;
			try {
				Thread.sleep(100);
			} catch(InterruptedException e) { }
		}
		TeaCupCode code = new TeaCupCode(a, b, c, d, f);
		return code;
	}
	
	private boolean binarySearch(Integer[] potions, Integer goal) {
		int low = 0, high = potions.length - 1, mid = 0;
		while(low <= high) {
			mid = (low + high) / 2;
			if(goal == mid) {
				return true;
			} else if(goal < mid) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}
		}
		return true;
	}
	
	public boolean checkPassword(String password) {
		String[] tokens = password.split(" ");
		int a = 0, b = 1;
		for(int i = 0; i < tokens.length; i++) {
			a += b;
			int currentToken = Integer.parseInt(tokens[i]);
			if(a != currentToken) {
				return false;
			}
			b += 2;
		}
		return b == 11;
	}
	
	public String calculatePuzzlePassword(List<Tumbler> o, List<Tumbler> m, List<Tumbler> i) {
		// SEE HOW HARD IT IS TO DEBUG LOOPS THAT HAVE WORTHLESS VARIABLE NAMES?
		// Change these variable names via refactoring. Also, backpack.calculateTumblers() doesn't do anything,
		// it's there so you have a good place to debug. The real juice is in return statement.
		for(int a = 0; a < o.size(); a++) {
			for(int b = 0; b < m.size(); b++) {
				for(int c = 0; c < i.size(); c++) {
					backpack.calculateTumblers(o.get(a), m.get(b), i.get(c));
				}
			}
		}
		return "000";
	}
	
	public SpawnPoint respawn() {
		return Checkpoint.returnCheckpoint();
	}

	public void finish() {
		System.out.println("Good job! You're now a debuggin' master!");
	}


}
